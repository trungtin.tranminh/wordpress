<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
 <meta name="description" content="<?php bloginfo('description'); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<?php wp_head();?>
</head>
<body>
<div class="wraper">
    <div class="header">
    <div class="site-title">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><h2><?php bloginfo('name'); ?></h2></a>
</div>
    </div>
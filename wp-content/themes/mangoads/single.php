<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();
?><div class="single-title"><?php the_title(); ?></div>
<?php the_content(); 
					
				endwhile;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php

get_footer();

 <?php
get_header(); ?>
<div class="container homepage">
<?php
if(have_posts()):
while ( have_posts() ) : the_post();
?>
<div class="post-list">
<div class="tittle-post"><a href="<?php the_permalink() ?>">
<?php the_title();?> </a></div>
<div class="content-post">
<?php the_content(); ?></div>
</div>
<?php endwhile; endif; ?>
</div>
<?php get_footer(); ?> 